PRAGMA foreign_keys=OFF;

DROP TABLE IF EXISTS materials;
DROP TABLE IF EXISTS ingredients;
DROP TABLE IF EXISTS recipe_lines;
DROP TABLE IF EXISTS cookies;
DROP TABLE IF EXISTS blocked;
DROP TABLE IF EXISTS pallets;
DROP TABLE IF EXISTS order_entries;
DROP TABLE IF EXISTS customers;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS deliveries;

PRAGMA foreign_keys=ON;


CREATE TABLE ingredients (
    ingredient          TEXT,
    unit                TEXT,
    PRIMARY KEY (ingredient)
);

CREATE TABLE materials (
    ingredient          TEXT,
    available_amount    REAL,
    PRIMARY KEY (ingredient),
    FOREIGN KEY (ingredient) REFERENCES ingredients(ingredient)
    CONSTRAINT ingredient_check CHECK (available_amount >= 0) ON CONFLICT ROLLBACK 
);

CREATE TABLE cookies (
    cookie              TEXT,
    PRIMARY KEY (cookie)
);

CREATE TABLE recipe_lines (
    ingredient          TEXT,
    amount              REAL,
    cookie              TEXT,
    PRIMARY KEY (cookie, ingredient),
    FOREIGN KEY (ingredient) REFERENCES ingredients(ingredient),
    FOREIGN KEY (cookie) REFERENCES cookies(cookie)
);

CREATE TABLE blocked (
    blocked_id          TEXT DEFAULT(lower(hex(randomblob(16)))),
    cookie              TEXT,
    blocked_from        DATE,
    blocked_to          DATE,
    PRIMARY KEY (blocked_id),
    FOREIGN KEY (cookie) REFERENCES cookies(cookie)
);


CREATE TABLE pallets (
    pallet_id             TEXT DEFAULT (lower(hex(randomblob(16)))),
    pallet_date           DATE DEFAULT (date('now')),
    cookie                TEXT,
    order_entry_id        TEXT,
    PRIMARY KEY (pallet_id),
    FOREIGN KEY (cookie) REFERENCES cookies(cookie),
    FOREIGN KEY (order_entry_id) REFERENCES order_entries(order_entry_id)
);

CREATE TABLE order_entries (
    order_entry_id  TEXT DEFAULT (lower(hex(randomblob(16)))),
    cookie          TEXT,
    order_id        TEXT,
    pallet_id       TEXT,
    nbr_pallets     INT,
    PRIMARY KEY (order_entry_id),
    FOREIGN KEY (cookie) REFERENCES cookies(cookie),
    FOREIGN KEY (order_id) REFERENCES orders(order_id),
    FOREIGN KEY (pallet_id) REFERENCES pallets(pallet_id)
);

CREATE TABLE orders (
    order_id            TEXT DEFAULT (lower(hex(randomblob(16)))),
    tot_nbr_pallets     INT,
    delivery_date       DATE,
    delivery_time       TIME,
    customer_name       TEXT,
    delivery_id         TEXT DEFAULT (NULL),
    PRIMARY KEY (order_id),
    FOREIGN KEY (customer_name) REFERENCES customers(customer_name),
    FOREIGN KEY (delivery_id) REFERENCES deliveries(delivery_id)
);

CREATE TABLE customers (
    customer_name    TEXT,
    address          TEXT,
    PRIMARY KEY (customer_name)
);

CREATE TABLE deliveries (
    delivery_id            TEXT DEFAULT (lower(hex(randomblob(16)))),
    actual_delivery_date   DATE, 
    order_id               TEXT,
    PRIMARY KEY (delivery_id),
    FOREIGN KEY (order_id) REFERENCES orders(order_id)
);